package com.keydrum.code;//Created by SyperG on 21.10.2017.

import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class SoundArrayList
{
    ArrayList<Sound> soundArrayList = new ArrayList<>();
    static String FILE_DIRECTORY = "src//com//keydrum//sounds//";
    Sound sound0 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-ClHat01.wav", KeyEvent.VK_Q);
    Sound sound1 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-ClHat02.wav", KeyEvent.VK_W);
	Sound sound2 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-Kick01.wav", KeyEvent.VK_R);
	Sound sound3 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-Kick02.wav", KeyEvent.VK_E);
	Sound sound4 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-Kick03.wav", KeyEvent.VK_T);
	Sound sound5 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-OpHat.wav", KeyEvent.VK_Y);
	Sound sound6 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-Shkr01.wav", KeyEvent.VK_A);
	Sound sound7 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-Shkr02.wav", KeyEvent.VK_S);
	Sound sound8 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-Shkr03.wav", KeyEvent.VK_D);
	Sound sound9 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-Snr01.wav", KeyEvent.VK_F);
	Sound sound10 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-Snr02.wav", KeyEvent.VK_G);
	Sound sound11 = new Sound(FILE_DIRECTORY + "CYCdh_VinylK1-Tamb.wav", KeyEvent.VK_H);
	
    public SoundArrayList()
    {
        soundArrayList.add(sound0);
        soundArrayList.add(sound1);
		soundArrayList.add(sound2);
		soundArrayList.add(sound3);
		soundArrayList.add(sound4);
		soundArrayList.add(sound5);
		soundArrayList.add(sound6);
		soundArrayList.add(sound7);
		soundArrayList.add(sound8);
		soundArrayList.add(sound9);
		soundArrayList.add(sound10);
		soundArrayList.add(sound11);
    }

    public Sound getSound(int key)
    {
        for(Sound s : soundArrayList)
        {
            if(s.getKey() == key)
            {
                return s;
            }
        }
        return new Sound();
    }
}
