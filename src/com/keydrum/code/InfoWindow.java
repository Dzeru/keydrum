package com.keydrum.code;//Created by SyperG on 24.10.2017.

import javax.swing.*;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static com.keydrum.code.RunProgram.mainWindow;

public class InfoWindow extends JFrame
{
    //private static volatile InfoWindow instanceInfoWindow;

    private static String infoText = "<html>Thank you for using KeyDrum!<br>" +
            "<br>Press the keys to play drums." +
            "<br>Enjoy your music. Enjoy your rhythm." +
            "<br><br>Best wishes," +
            "<br>Dzeru" +
            "</html>";

    public InfoWindow()
    {
        super("Info");
        super.setSize(new Dimension(700, 400));
        super.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        super.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                InfoWindow.super.setVisible(false);
                mainWindow.getContentPane().setFocusable(true);
            }
        });



        ColorConstants colorConstants = new ColorConstants();
        super.getContentPane().setBackground(colorConstants.backgroundColor);

        JLabel infoLabel = new JLabel(infoText);

        infoLabel.setOpaque(true);
        infoLabel.setBackground(colorConstants.getBackgroundColor());
        infoLabel.setForeground(colorConstants.getFontColor());
        infoLabel.setFont(colorConstants.getFontSmall());

        super.getContentPane().setLayout(new BorderLayout());
        super.getContentPane().add(infoLabel, BorderLayout.CENTER);
    }
/*
    public static InfoWindow getInstance()
    {
        InfoWindow iw = new InfoWindow();
        if(instanceInfoWindow == null)
        {
            synchronized(InfoWindow.class)
            {
                instanceInfoWindow = iw;
            }
        }

        return instanceInfoWindow;
    }
*/
}
