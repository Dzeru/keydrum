package com.keydrum.code;//Created by SyperG on 21.10.2017.

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.metal.MetalScrollBarUI;
import javax.swing.table.TableColumn;
import javax. swing.table.TableCellRenderer;
import javax.swing.table.DefaultTableCellRenderer;

import java.awt.*;
import java.awt.event.*;

import java.util.Vector;

import static com.keydrum.code.RunProgram.mainWindow;


/*
TODO:
1. Create the table for the panelWithDrums
2. Write the Help and About window
3. Resolve the problem of loosing focus on panelWithDrums
*/

public class MainWindow extends JFrame
{
    static InfoWindow infoWindow = new InfoWindow();

    public MainWindow()
    {
        super("KeyDrum");

        ColorConstants colorConstants = new ColorConstants();
        Dimension sizeOfFrame = Toolkit.getDefaultToolkit().getScreenSize();

        GridBagLayout gridBagLayout = new GridBagLayout();
        GridBagConstraints gridBagConstraints = new GridBagConstraints();

        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;

        super.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        super.getContentPane().setBackground(colorConstants.getBackgroundColor());
        super.getContentPane().setLayout(gridBagLayout);
        super.setPreferredSize(sizeOfFrame);

        JPanel panelWithDrums = new JPanel();
        panelWithDrums.setBackground(colorConstants.getBackgroundColor());
        panelWithDrums.setLayout(new BorderLayout());
        panelWithDrums.setFocusable(true);

        panelWithDrums.addFocusListener(new FocusListener()
        {
            @Override
            public void focusGained(FocusEvent e)
            {
                panelWithDrums.setFocusable(true);
                System.out.println("fg");
            }

            @Override
            public void focusLost(FocusEvent e)
            {
                panelWithDrums.setFocusable(true);
                System.out.println("fl");
            }
        });

        panelWithDrums.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyPressed(KeyEvent e)
            {
                int k = e.getKeyCode();
                Sound.playSound(k);
            }
        });

        super.getContentPane().add(panelWithDrums);

        ListsForTable listsForTable = new ListsForTable();

        JTable tableWithDrums = new JTable(listsForTable.getListOfSounds(), listsForTable.getHeader());
        tableWithDrums.setDefaultRenderer(Object.class, new CellRenderer());
        tableWithDrums.setGridColor(colorConstants.getFontColor());
        tableWithDrums.setOpaque(true);
        tableWithDrums.setCellSelectionEnabled(false);
        tableWithDrums.setRowHeight(50);

        TableColumn column = tableWithDrums.getColumnModel().getColumn(0);
        column.setHeaderRenderer(new HeaderRenderer());
        column = tableWithDrums.getColumnModel().getColumn(1);
        column.setHeaderRenderer(new HeaderRenderer());
        column = tableWithDrums.getColumnModel().getColumn(2);
        column.setHeaderRenderer(new HeaderRenderer());

        int heightOfViewportSize = (int)sizeOfFrame.getHeight() - 100;

        panelWithDrums.setSize(new Dimension((int)sizeOfFrame.getWidth() - 200, heightOfViewportSize));
        tableWithDrums.setPreferredScrollableViewportSize(new Dimension((int)sizeOfFrame.getWidth() - 200, heightOfViewportSize));

        JPanel panelForCorner = new JPanel();
        panelForCorner.setBackground(colorConstants.getBackgroundColor());
        JScrollPane scrollPaneOfTableWithDrums = new JScrollPane(tableWithDrums, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPaneOfTableWithDrums.getViewport().setOpaque(true);
        scrollPaneOfTableWithDrums.setWheelScrollingEnabled(true);
        scrollPaneOfTableWithDrums.setCorner(JScrollPane.UPPER_RIGHT_CORNER, panelForCorner);
        scrollPaneOfTableWithDrums.setBorder(new EmptyBorder(0, 0, 0, 0));

        scrollPaneOfTableWithDrums.getVerticalScrollBar().setUI(new MetalScrollBarUI()
        {
            @Override
            protected void paintThumb(Graphics g, JComponent c, Rectangle tb)
            {
                g.setColor(colorConstants.getFontColor());
                g.fillRect(tb.x, tb.y, tb.width, tb.height);
            }

            @Override
            protected void paintTrack(Graphics g, JComponent c, Rectangle tb)
            {
                g.setColor(colorConstants.getBackgroundColor());
                g.fillRect(tb.x, tb.y, tb.width, tb.height);
            }

            @Override
            protected JButton createDecreaseButton(int orientation)
            {
                JButton db = new JButton("+");
                db.setOpaque(true);
                db.setBackground(colorConstants.getBackgroundColor());
                db.setForeground(colorConstants.getFontColor());
                db.setBorder(null);
                db.addActionListener(scrollListener);

                return db;
            }

            @Override
            protected JButton createIncreaseButton(int orientation)
            {
                JButton db = new JButton("-");
                db.setOpaque(true);
                db.setBackground(colorConstants.getBackgroundColor());
                db.setForeground(colorConstants.getFontColor());
                db.setBorder(null);
                db.addActionListener(scrollListener);

                return db;
            }
        });

        panelWithDrums.add(scrollPaneOfTableWithDrums, SwingConstants.CENTER);

        JButton infoButton = new JButton("Info");
        infoButton.setFont(colorConstants.getFontLarge());
        infoButton.setBorder(new EmptyBorder(5, 5, 0, 0));
        infoButton.setOpaque(true);
        infoButton.setBackground(colorConstants.getBackgroundColor());
        infoButton.setForeground(colorConstants.getFontColor());
        infoButton.setPreferredSize(new Dimension(100, 100));

        infoButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                //InfoWindow.getInstance();
                infoWindow.setVisible(true);
            }
        });

        gridBagConstraints.anchor = GridBagConstraints.SOUTHEAST;
        gridBagLayout.setConstraints(infoButton, gridBagConstraints);
        super.getContentPane().add(infoButton);

        super.setExtendedState(MAXIMIZED_BOTH);
        super.setVisible(true);
    }
}

class CellRenderer extends DefaultTableCellRenderer implements TableCellRenderer
{
    ColorConstants colorConstants = new ColorConstants();
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        this.setText(value.toString());
        this.setOpaque(true);
        this.setFont(colorConstants.getFontSmall());
        this.setBackground(colorConstants.getBackgroundColor());
        this.setForeground(colorConstants.getFontColor());
        this.setHorizontalAlignment(SwingConstants.CENTER);
        this.setVerticalAlignment(SwingConstants.CENTER);

        return this;
    }
}

class HeaderRenderer extends DefaultTableCellRenderer implements TableCellRenderer
{
    ColorConstants colorConstants = new ColorConstants();
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        this.setText(value.toString());
        this.setOpaque(true);
        this.setFont(colorConstants.getFontLarge());
        this.setBackground(colorConstants.getBackgroundColor());
        this.setForeground(colorConstants.getFontColor());
        this.setHorizontalAlignment(SwingConstants.CENTER);
        this.setVerticalAlignment(SwingConstants.CENTER);
        this.setBorder(new LineBorder(colorConstants.getFontColor()));

        return this;
    }
}

class ListsForTable
{
    int countOfRow = 12;
    Vector<Vector<String>> listOfSounds = new Vector<Vector<String>>();
    Vector<String> header = new Vector<>();

    ListsForTable()
    {
        for(int i = 0; i < countOfRow; i++)
        {
            listOfSounds.add(new Vector<>());
        }

        listOfSounds.get(0).add("Q");
        listOfSounds.get(0).add("Ts");
        listOfSounds.get(0).add("Closed Hat 1");

        listOfSounds.get(1).add("W");
        listOfSounds.get(1).add("Ts higher");
        listOfSounds.get(1).add("Closed Hat 2");

        listOfSounds.get(2).add("E");
        listOfSounds.get(2).add("Tum");
        listOfSounds.get(2).add("Kick 1");

        listOfSounds.get(3).add("R");
        listOfSounds.get(3).add("Tum higher");
        listOfSounds.get(3).add("Kick 2");

        listOfSounds.get(4).add("T");
        listOfSounds.get(4).add("Tum the highest");
        listOfSounds.get(4).add("Kick 3");

        listOfSounds.get(5).add("Y");
        listOfSounds.get(5).add("Tsss");
        listOfSounds.get(5).add("Open Hat");

        listOfSounds.get(6).add("A");
        listOfSounds.get(6).add("Shu");
        listOfSounds.get(6).add("Shaker 1");

        listOfSounds.get(7).add("S");
        listOfSounds.get(7).add("St");
        listOfSounds.get(7).add("Shaker 2");

        listOfSounds.get(8).add("D");
        listOfSounds.get(8).add("Sh");
        listOfSounds.get(8).add("Shaker 3");

        listOfSounds.get(9).add("F");
        listOfSounds.get(9).add("Dum");
        listOfSounds.get(9).add("Snare 1");

        listOfSounds.get(10).add("G");
        listOfSounds.get(10).add("Dum");
        listOfSounds.get(10).add("Snare 2");

        listOfSounds.get(11).add("H");
        listOfSounds.get(11).add("Tsy");
        listOfSounds.get(11).add("Tambourine");

        header.add("Key");
        header.add("Sound");
        header.add("Drum");
    }

    public Vector<Vector<String>> getListOfSounds()
    {
        return listOfSounds;
    }

    public Vector<String> getHeader()
    {
        return header;
    }

}
