package com.keydrum.code;//Created by SyperG on 24.10.2017.

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.awt.FontFormatException;

public class ColorConstants
{
    Color backgroundColor;
    Color fontColor;
    String fontDirectory;
    Font textFont;
    Font textFontSmall;
    Font textFontLarge;

    public ColorConstants()
    {
        backgroundColor = new Color(129, 199, 132);
        fontColor = new Color(255, 255, 255);

        try
        {
            fontDirectory = new File("src\\com\\keydrum\\Raleway-Regular.ttf").getAbsolutePath();
            textFont = Font.createFont(Font.TRUETYPE_FONT, new File(fontDirectory));
            textFontSmall = textFont.deriveFont(20f);
            textFontLarge = textFont.deriveFont(26f);

        }
        catch(IOException | FontFormatException e)
        {
            e.printStackTrace();
            System.out.println("Ooops! There is some problem with font!");
        }
    }

    public Color getBackgroundColor()
    {
        return backgroundColor;
    }

    public Color getFontColor()
    {
        return fontColor;
    }

    public Font getFontSmall()
    {
        return textFontSmall;
    }

    public Font getFontLarge()
    {
        return textFontLarge;
    }
}