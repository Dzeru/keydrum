package com.keydrum.code;//Created by SyperG on 08.10.2017.

import javax.swing.SwingUtilities;

public class RunProgram
{
    static MainWindow mainWindow;

    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                mainWindow = new MainWindow();
                System.out.println(mainWindow.getFocusOwner());
            }
        });
    }
}
