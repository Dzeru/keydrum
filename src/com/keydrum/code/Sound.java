package com.keydrum.code;//Created by SyperG on 17.10.2017

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.*;

/*
TODO:
1. Make GUI error message
2. Integrate playSound() with KeyListener
 */

public class Sound
{
	File sound;
	int key;

    public Sound(String seso, int k)
    {
        setSound(seso);
        setKey(k);
    }

    public Sound()
    {

    }
	
	public void setSound(String seso)
	{
		sound = new File(seso);
	}
	
	public File getSound()
	{
		return sound;
	}
	
	public void setKey(int k)
	{
		key = k;
	}
	
	public int getKey()
	{
		return key;
	}

	public static void playSound(int key)
    {
        try
        {
            SoundArrayList soundArrayList = new SoundArrayList();
            Sound playedSound = soundArrayList.getSound(key);

            AudioInputStream ais = AudioSystem.getAudioInputStream(playedSound.getSound());

            Clip clip = AudioSystem.getClip();

            clip.open(ais);

            clip.setFramePosition(0);
            clip.start();

        }
        catch(IOException | LineUnavailableException | UnsupportedAudioFileException e)
        {
            e.printStackTrace();
            System.err.println("Oops! There are some problem with sound system");
        }
    }

}